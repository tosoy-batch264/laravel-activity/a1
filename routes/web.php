<?php

use Illuminate\Support\Facades\Route;

// Link to the PostController File.
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Define a route where in a view to create a post will be returned to the user.
Route::get('/posts/create', [PostController::class, 'create']);

// Define a route where in the form data will be sent via POST method to the /post URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// Define a route that will return a view containing all posts.
Route::get('/posts', [PostController::class, 'index']);

Route::get('/', [PostController::class, 'welcome']); // Activity

// Define a route that will return a view containing only the authenticated user's posts.
Route::get('/myPosts', [PostController::class, 'myPosts']);

// Define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user.
Route::get('/posts/{id}', [PostController::class, 'show']);

// Define a route for viewing the edit post form - S03 Activity Solution
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// Define a route that will overwrite an existing post with matching URL parameter ID via PUT method.
Route::put('/posts/{id}', [PostController::class, 'update']);

// Define a route tha will delete a post of the matching URL parameter ID.
// Route::delete('/posts/{id}', [PostController::class, 'destroy']);

Route::put('/posts/{id}', [PostController::class, 'archive']);


// Define a route that will allow the users to like post/s.
Route::put('/posts/{id}/like', [PostController::class, 'like']);


// Final Activity
Route::post('/posts/{id}/comment', [PostController::class, 'comment'])->name('posts.comment');



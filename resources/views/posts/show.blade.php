@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted mb-3">Created At: {{$post->created_at}}</p>
            
            <p class="card-text">{{$post->content}}</p>

            {{-- @if(Auth::id() != $post->user_id)
                <form action="/posts/{{$post->id}}/like" class="d-inline" method="POST">
                    @method('PUT')
                    @csrf
                    @if($post->like && $post->like->contains("user_id", Auth::user()->id)) 
                    {
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    }
                    @else 
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif --}}


            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains("user_id", Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif            
                </form>
            @endif

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
                Add Comment
            </button>
            
            <!-- Modal -->
            <div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="commentModalLabel">Add Comment</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <form action="{{ route('posts.comment', $post->id) }}" method="post">
                        @csrf
                        <div class="form-group">
                        <textarea class="form-control" name="content" rows="3" placeholder="Enter comment"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    </div>
                </div>
                </div>
            </div>
  
            
            <div class="mt-3">
                <a href="/myPosts" class="card-link">View all posts</a>
            </div>
        </div>
    </div>

    <h2 class="mt-5">Comments</h2>
    <div class="card">
        <div class="card-body text-center">
            
            @if($post->comments->count() == 0)
        <p>No comments yet.</p>
    @else
        <ul class="list-unstyled">
            @foreach($post->comments as $comment)
                <li>
                    <div class="border ">
                        <h4 class="card-subtitle mt-5">{{ $comment->content }}</h4>
                        <p class="card-subtitle text-muted mt-1 mb-5">Posted by: {{ $comment->user->name }}</p>
                    </div>
                </li>
            @endforeach
        </ul>
    @endif
        </div>
    </div>

@endsection

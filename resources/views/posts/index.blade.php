@extends('layouts.app')

@section('content')

    @if(count($posts) > 0) 
    
        @foreach ($posts as $post)
            <div class="card text-center mb-3">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                    <p class="card-subtitle text-muted mb-3">Status: {{$post->isActive}}</p>
                </div>

                {{-- Check if there is an authenticated user to prevent our web app from throwing an error when user is login --}}
                @if (Auth::user()) 
                    @if (Auth::user()->id == $post->user_id)
                        <div class="mb-3 flex">

                            <form action="/posts/{{$post->id}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit Post</a>

                                <form action="/posts/{{$post->id}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" name="isAactive" value="{{ $post->isActive ? '1' : '0' }}">
                                    <button type="submit" name="toggle_active" class="btn {{ $post->isActive ? 'btn-danger' : 'btn-success' }}">
                                        {{ $post->isActive ? 'Deactivate' : 'Activate' }}
                                    </button>
                                </form>
                            </form>
                        </div>
                    @endif
                @endif
            </div>
        @endforeach

    @else
        <div>
            <h2>There are no posts to show</h2>
            <a href="/posts/create" class="btn btn-info">Create Post</a>
        </div>
    @endif

@endsection
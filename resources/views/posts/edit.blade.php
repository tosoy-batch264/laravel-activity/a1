@extends('layouts.app')

@section('content')
    <form method="POST" action="/posts/{{$post->id}}">
        @csrf
        @method('PUT')

        <h1>Update Post</h1>
        <div class="form-group">
            <label for="title">Title: </label>
            <input type="text" name="title" class="form-control shadow-none" value="{{$post->title}}">
        </div>
        <div class="form-group">
            <label for="content">Content: </label>
            <textarea class="form-control shadow-none" id="content" name="content" rows="3">{{$post->content}}</textarea>
        </div>

        @if (Auth::user()->id === $post->user_id)
            <div class="mt-2">
                <button type="submit" class="btn btn-primary">Update Post</button>
            </div>
        @endif
    </form>
@endsection
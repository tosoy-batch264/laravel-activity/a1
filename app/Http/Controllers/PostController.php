<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Auth (middleware) - access the authenticated user via Auth Facades.
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // Action to return a view containing a form for blog post creation.
    public function create() 
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        if (Auth::user()) 
        {
            // Instantiate a new Post object from the Post model
            $post = new Post;

            // Define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');

            // Get the id of the authenticated user and set it as the foreign key user_id of the new post.
            $post->user_id = (Auth::user()->id);

            // Save this post object into the database.
            $post->save();

            return redirect('/posts');
        }
        else 
        {
            return redirect('login');
        }
    }

    // Mini Activity.
    //1. In the create_posts_table.php add an isActive field with a Boolean data type and a default value "true"
    //2. run command: to refresh the migrations and to add the isActive field into the posts table.
    //3. Refactor the index() in PostController so that it will only return the active posts from the posts table regardless of the user.

    // Action that will return a view showing all blog posts.
    public function index() 
    {
        $posts = Post::where('isActive', true)->get();
        return view('posts.index')->with('posts', $posts);
    }

    // Action for showing only the posts authored by the authenticated user.
    public function myPosts() 
    {
        if (Auth::user()) 
        {
            $posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $posts);
        }
        else {
            return redirect('/login');
        }
    }

    public function  welcome() 
    {
        $posts = Post::inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    // Action that will return a view showing a specific post using the URl parameter $id.
    public function show($id) 
    {
        // $post = Post::find($id);
        $post = Post::with('comments.user')->findOrFail($id);
        return view('posts.show')->with('post', $post);
    }

    public function edit($id) 
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    // This is an action to update a specific posts by an authenticated user.
    public function update(Request $request, $id) 
    {
        $post = Post::find($id);

        // If authenticated user's ID is the same as the post's user_id.
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->save();
        
        return view('posts.show')->with('post', $post);
    }

    // Define action/functioon for deleting post/s
    public function destroy($id) 
    {
        $post = Post::find($id);

        if (Auth::user()->id === $post->user_id)
        {
            $post->delete();
        }
        return redirect('/posts');
    }

    // This is an action to archive a post.
    public function archive($id) 
    {
        $post = Post::find($id);

        if (request()->has('toggle_active')) {
            $post->isActive = !$post->isActive; // toggle the isActive attribute
        }
    
        $post->save();
        return redirect('/posts');
    }

    public function like($id) 
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        // If an authenticated user is not the post author
        if ($post->user_id != $user_id) 
        {
            // This checks if a post like has been made by a certain user before.
            if ($post->likes->contains('user_id', $user_id)) 
            {
                // Delete the like made by this user to unlike this post.
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            }
            else 
            {
                // Create a new like record to like a speciic post.
                // Instantiate a new PostLike object from the PostLike model.
                $postLike = new PostLike;

                // Define the properties of the $postLike object.
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                // Save this $postLike object into the database.
                $postLike->save();
            }

            // Redirect the user back to the same post.
            return redirect("/posts/$id");
        }
    }


    // Final Activity
    public function comment(Request $request, $id)
{
    $validatedData = $request->validate([
        'content' => 'required',
    ]);

    $post = Post::find($id);

    if (!$post) {
        abort(404);
    }

    $comment = new PostComment;
    $comment->post_id = $id;
    $comment->user_id = Auth::user()->id;
    $comment->content = $validatedData['content'];
    $comment->save();

    return redirect()->back()->with('success', 'Comment added successfully.');
}



}
